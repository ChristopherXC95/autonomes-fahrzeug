#OPTION
#Aendert zwar Prozessnamen, in PIDfinder wird allerdings 
#nach ausgefuehrtem Dateiname gesucht
def set_proc_name(newname):
    from ctypes import cdll, byref, create_string_buffer
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(len(newname)+1)
    buff.value = newname
    libc.prctl(15, byref(buff), 0, 0, 0)
#nicht mehr als 14 zeichen, sonst wird standard behalten
set_proc_name('testballtrace')


print ('Modi:')
import time,sys
value=0
while 1:
    value=value+1
    time.sleep(1)
    print ("Ball", value)
    
