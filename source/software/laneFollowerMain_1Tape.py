import cv2
import numpy as np
import math
from picar import front_wheels, back_wheels
from picar.SunFounder_PCA9685 import Servo
import picar
import datetime
import time
import signal
import sys

# display these images:
showAny = False
showOriginal = 0
showColorMasked = 1
showCannyEdges = 0
showCannyEdgesHalf = 1
showLineSegments = 1
showLaneLines = 1
showHeadingLine = 0

# green tape/marker dependend parameters
markerColorLower = np.array([40, 100, 100])
markerColorUpper = np.array([90, 255, 255])
cannyThreshold1 = 200
cannyThreshold2 = 400

# camera dependend parameters
houghLineDet_rho = 1 # use every pixel
houghLineDet_angle = np.pi /180 # angle precision in radians (1 degree)
houghLineDet_minThreshold = 10  # minimal of votes
houghLineDet_minLineLength=10 # minimum length of line to not be ignored
houghLineDet_maxLineGap=20 # maximum spacing between segments to count as a single line

#stabilizer
curr_steering_angle = 90

picar.setup()
# setup variables for piCar motors
move = False
base_speed=60
bw = back_wheels.Back_Wheels()
fw = front_wheels.Front_Wheels()
bw.speed = 0
fw.turn(90)
fw.offset = 0
motor_speed = 60

#setup or camera servos
pan_servo = Servo.Servo(1)
tilt_servo = Servo.Servo(2)
pan_servo.write(90)
tilt_servo.write(80)

#moving average
index= 0
last_time = datetime.datetime.now()
MA_array = np.full((5), 90)
old_s_angle = 90
no_detection = 0
stabilized_angle = 90
search_tape_r = 0
search_tape_l = 0

def main():
    global curr_steering_angle, motor_speed, MA_array, old_s_angle, no_detection, stabilized_angle, search_tape_r, search_tape_l
    print("Line Follower Started!")
    cam = cv2.VideoCapture(-1)
    SCREEN_WIDTH = 320 #160
    SCREEN_HIGHT = 240 #120
    cam.set(cv2.CAP_PROP_FRAME_HEIGHT, SCREEN_HIGHT)
    cam.set(cv2.CAP_PROP_FRAME_WIDTH, SCREEN_WIDTH)
    no_detection2 = 0
    
    move = getMovePermission()
    (markerColorLower, markerColorUpper) = getColorBoundsFromFile()
    
    while( cam.isOpened() and not signalHandler.kill_now):
    #while(not signalHandler.kill_now):
        try:
            _, frame = cam.read()
        except:
            print('Camera connection lost!')
            signalHandler.kill_now = True
            continue
        if frame is None:
            print('Camera connection lost, framebuffer!')
            signalHandler.kill_now = True
            continue
        show_image('Original', frame, showOriginal)

        # filter for marker/tape color
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        mask = cv2.inRange(hsv, markerColorLower, markerColorUpper)
        show_image("Color mask", mask, showColorMasked)

        # detect edges
        edges = cv2.Canny(mask, cannyThreshold1, cannyThreshold2)
        show_image("Edges", edges, showCannyEdges)

        # mask out upper area (nothing of interest shall be detected there)
        edgesTopHalf = np.zeros(edges.shape, np.uint8)
        h,w = edges.shape
        edgesTopHalf[int(h/2):h,0:w] = edges[int(h/2):h,0:w]
        show_image("Edges masked", edgesTopHalf, showCannyEdgesHalf)

        # find line segments using Hough Line Detection and store in array
        line_segments = detect_lines(edgesTopHalf)
        if line_segments is None or search_tape_r or search_tape_l:
            if search_tape_r:
                search_tape_r = 0
                if line_segments is not None:
                    fw.turn(135)
                    if move == 1:
                        bw.speed = 30
                        bw.backward()
                    pan_servo.write(90)
                    time.sleep(1)
                    bw.stop()
                else:
                    search_tape_l = 1
                    pan_servo.write(135)
                    time.sleep(1)
                    purge_frame_buffer(cam)
            elif search_tape_l:
                search_tape_l = 0
                if line_segments is not None:
                    fw.turn(45)
                    if move == 1:
                        bw.speed = 30
                        bw.backward()
                    pan_servo.write(90)
                    time.sleep(1)
                    bw.stop()
                else:
                    pan_servo.write(90)
            elif no_detection>20:
                bw.speed = 0
                bw.stop()
                #break
                #look around for tape
                pan_servo.write(45)
                time.sleep(1)
                purge_frame_buffer(cam)
                search_tape_r = 1
                print("No tape detected")
            else:
                no_detection = no_detection+1


        else:
            no_detection = 0
            print("Tape detected")
            show_image('Line segments',display_lines(frame, line_segments), showLineSegments)

            dest_x, dest_y = calc_dest_x(line_segments)
            new_s_angle = calc_t_angle4(dest_x, dest_y, SCREEN_WIDTH, SCREEN_HIGHT)
            motor_speed = speed_adaption(new_s_angle, base_speed)
            stabilized_angle = stabilize_steering_angle2(
              stabilized_angle,
              new_s_angle,
              max_angle_deviation=5)
            fw.turn(stabilized_angle)
            # smoothen steering using moving average filtering
            # commented out because stabilize steering angle seems to be enough
    #         MA_array = insert_MA_array (MA_array, steering_angle, motor_speed)
    #         new_s_angle = np.average(MA_array)
    #         if abs(new_s_angle-old_s_angle)>3:
    #             new_s_angle = old_s_angle+3*((new_s_angle-old_s_angle)/abs(new_s_angle-old_s_angle))

            #fw.turn(new_s_angle)
            #old_s_angle = new_s_angle
            
            if move==1:
                if motor_speed != 0:
                    bw.speed = motor_speed
                    bw.backward()
                else:
                    bw.stop()
            else:
                bw.speed = 0
                bw.stop()

            
        if cv2.waitKey(1) & 0xFF == ord('q') :
            bw.stop()
            break

    pan_servo.write(90)
    tilt_servo.write(80)
    fw.turn(90)
    bw.stop()
    cv2.destroyAllWindows()
    print("Line Follower Stopped")

def show_image(title,frame, DOSHOW=1):
    if(DOSHOW and showAny):
        cv2.imshow(title, frame)

def purge_frame_buffer(cam):
    # caputre new images so the internal camera buffer gets overwritten
    # sleep instruction messes with the buffering, so this is needed
    for i in xrange(4):
        cam.grab()

# tweaked from tutorial page, adjust values to used camera!
def detect_lines(frame):
    # perform Hough Line Detection
    line_segments = cv2.HoughLinesP(frame, houghLineDet_rho, houghLineDet_angle, houghLineDet_minThreshold,
                                    np.array([]), minLineLength=houghLineDet_minLineLength,
                                    maxLineGap=houghLineDet_maxLineGap)
    return line_segments

# from tutorial, unchanged
def display_lines(frame, lines, line_color=(0, 255, 0), line_width=10):
    # from tutorial page
    # returns given image with lines drawn onto it
    line_image = np.zeros_like(frame)
    if lines is not None:
        for line in lines:
            for x1, y1, x2, y2 in line:
                cv2.line(line_image, (x1, y1), (x2, y2), line_color, line_width)
    line_image = cv2.addWeighted(frame, 0.8, line_image, 1, 1)
    return line_image

# from tutorial, unchanged
def display_heading_line(frame, steering_angle, line_color=(0, 0, 255), line_width=5, ):
    # UNUSED, created for two lane marker navigation
    heading_image = np.zeros_like(frame)
    height, width, _ = frame.shape

    # figure out the heading line from steering angle
    # heading line (x1,y1) is always center bottom of the screen
    # (x2, y2) requires a bit of trigonometry

    # Note: the steering angle of:
    # 0-89 degree: turn left
    # 90 degree: going straight
    # 91-180 degree: turn right 
    steering_angle_radian = steering_angle / 180.0 * math.pi
    x1 = int(width / 2)
    y1 = height
    x2 = int(x1 - height / 2 / math.tan(steering_angle_radian))
    y2 = int(height / 2)

    cv2.line(heading_image, (x1, y1), (x2, y2), line_color, line_width)
    heading_image = cv2.addWeighted(frame, 0.8, heading_image, 1, 1)

    return heading_image
    
def stabilize_steering_angle2(
          curr_steering_angle, 
          new_steering_angle,  
          max_angle_deviation=1):
    """
    Using last steering angle to stabilize the steering angle
    if new angle is too different from current angle, 
    only turn by max_angle_deviation degrees
    """    
    angle_deviation = new_steering_angle - curr_steering_angle
    if abs(angle_deviation) > max_angle_deviation:
        stabilized_steering_angle = int(curr_steering_angle
            + max_angle_deviation * angle_deviation / abs(angle_deviation))
    else:
        stabilized_steering_angle = new_steering_angle
    return stabilized_steering_angle
    
def insert_MA_array (array, new_value, m_speed):
    global index
    current_time = datetime.datetime.now()
    if motor_speed !=0 :
        time_gone = (last_time-current_time).microseconds
        space = m_speed * time_gone
        #only add when drove at least 1s with speed 20
        if space > 20000:
            array[index] = new_value
            if index==(len(array)-1):
                index = 0
            else:
                index= index+1
    return array
    
def calc_t_angle3(destination_x, s_width):
    # UNUSED, replaced by calc_t_angle4
    #calc turning angle with given destination_x and screen_width
    deviation = s_width/2 - destination_x
    
    if(abs(deviation)<20):
        angle= 90
        angle_text = "forward"
    elif (deviation > 0):
        #turn left
        if(abs(deviation)<(s_width/4)):
            angle = 75
            angle_text = "soft_left"
        else:
            angle = 30
            angle_text = "hard_left"
    elif (deviation < 0 ):
        #turn right
        if(abs(deviation)<(s_width/4)):
            angle = 100
            angle_text = "soft_right"
        else:
            angle = 150
            angle_text = "hard_right"
        
    return angle, angle_text
    
def calc_t_angle4(destination_x, destination_y, s_width, s_hight):
    left_offset = -15
    y_offset = s_hight-destination_y
    x_offset = destination_x - 0.5*s_width
    if x_offset >=0:
        angle_rad = math.atan(y_offset/x_offset)
        angle_deg = int(angle_rad * 180.0 / math.pi)
        angle_deg = 180-angle_deg
    else:
        angle_rad = math.atan(y_offset/abs(x_offset))
        angle_deg = int(angle_rad * 180.0 / math.pi)
    return angle_deg

def calc_dest_x(line_segments):
    #compare found line_segments and save the x-value of the one with the lowest y (lowest y = highest position on picture)
    min_y = 1000
    dest_x = -1
    
    for line_segment in line_segments:
        for x1, y1, x2, y2 in line_segment:
            if y1 <= y2:
                if y1 <= min_y:
                    min_y = y1
                    dest_x = x1
            else:
                if y2 <= min_y:
                    min_y = y2
                    dest_x = x2
    
    return dest_x, min_y

def speed_adaption(angle, base_speed):
    if(angle>60 and angle<120):
        motor_speed = base_speed
    elif(angle>120 or angle<60):
        motor_speed = int(base_speed/1.25)
    else:
        motor_speed = int(base_speed/1.25)
    if motor_speed < 20:
        motor_speed = 20
        
    return motor_speed

def getColorBoundsFromFile():
    with open("/home/pi/logs/color.log","r") as fp:
        content = fp.read()
    content = content.replace('hsv(','')
    content = content.replace(')','')
    content = content.replace('%', '')
    h,s,v = [int(x) for x in content.split(',')]
    with open("/home/pi/logs/balltolerance.log","r") as fp:
        content = fp.read()
    tol = int(content)/100.0
    
    h_low = max(0, (h/2 - tol * 90))
    h_high = min(255, h/2 + tol * 90)
    s_low = max(0, (s/100.0 - tol) * 255 )
    s_high = min(255, (s/100.0 + tol) * 255)
    v_low = max(0, (v/100.0 - tol) * 255 )
    v_high = min(255, (v/100.0 + tol) * 255)
    colLower = (h_low,s_low,v_low)
    colHigher = (h_high, s_high, v_high)
    return (colLower,colHigher)

def getMovePermission():
    with open("/home/pi/logs/moveSwitch.log",'r') as fp:
        content = fp.read()
    if content == "false":
        return False
    else:
        return True

class SignalHandlerClass:
    kill_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exitHard)
        signal.signal(signal.SIGTERM, self.exitSavely)

    def exitHard(self, signum, frame):
        bw.stop()
        sys.exit()

    def exitSavely(self, signum, frame):
        print('Shutting down savely...')
        self.kill_now = True

if __name__ == '__main__':
    signalHandler = SignalHandlerClass()
    main()
