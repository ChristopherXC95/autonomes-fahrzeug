#Kevin Diener 19.02.2020 Hochschule Pforzheim
#Prueft ob Prozesse bereits laufen und schickt bei Bedarf einen SIGINT
import os,sys,signal,time,subprocess

def preexec():
    os.setpgrp();

#Sucht ausfuehrendes Programm unter dem Programmnamen (Nicht Prozessname)
def getPID(myprocess):
    cmd = "ps ax|grep -v grep | grep %s" % (myprocess)
   
    output = ""
    f=os.popen(cmd)
    for i in f.readlines():
        output = output + i
    if output == "":
        #-- Kein Prozess gefunden
        return("NOPID")
    else:
        #-- Prozess existiert, suche PID raus
        begin = 0
        y = True
        while y==True:
            if output[begin] == " ":
                # Leerzeichen gefunden, PID beginnt noch nicht
                begin += 1
            else:
                # Kein Leerzeichen gefunden, PID beginnt hier
                y = False
        end = begin
        y = True
        while y==True:
            if output[end] == " ":
                # Leerzeichen gefunden, PID endet hier, breche ab
                y= False
            else:
                end += 1
        return(output[begin:end])

ballTrackPID = getPID("ball_tracker_V3.py")
if ballTrackPID is not "NOPID":
    subprocess.Popen(["kill","-SIGTERM",ballTrackPID],shell=False)
    print("Ballverfolgung beendet")
else:
    print("PID Ballverfolgung nicht gefunden")
lineFollowPID = getPID("laneFollowerMain_1Tape.py")
if lineFollowPID is not "NOPID":
    subprocess.Popen(["kill","-SIGTERM",lineFollowPID],shell=False)
    print("Linienfolger beendet")
else:
    print("PID Linienfolger nicht gefunden")

# try:
#     #Pruefung ob bereits ein Prozess laeuft, wenn ja wird SIGTERM gesendet
#     if sys.argv[1]=="0":
#         try:
#             #print ("Balltrace beenden mit PID:",int(getPID("testballtrace")))
#             subprocess.Popen(["kill","-SIGTERM",getPID("testballtrace")],shell=False)
#         except:
#             print("PID Ballverfolgung nicht gefunden")
#         try:
#             subprocess.Popen(["kill","-SIGTERM",getPID("testlinetrace")],shell=False)
#         except:
#             print("PID Linenverfolgung nicht gefunden")
#     #Linienverfolgung beenden, Ballverfolgung starten
#     if sys.argv[1]=="1":
#         try:
#             subprocess.Popen(["kill","-SIGTERM",getPID("testlinetrace")],shell=False)
#         except:
#             print("PID Linienverfolgung nicht gefunden")
#         try:
#             subprocess.Popen(["kill","-SIGTERM",getPID("testballtrace")],shell=False)
#         except:
#             print("PID Ballverfolgung nicht gefunden")
#     if sys.argv[1]=="2":
#         try:
#             subprocess.Popen(["kill","-SIGTERM",getPID("testlinetrace")],shell=False)
#             print ("Linienverfolgung beendet")
#         except:
#             pass
#         try:
#             subprocess.Popen(["kill","-SIGTERM",getPID("testballtrace")],shell=False) 
#             print ("Ballverfolgung beendet")
#         except:
#             pass
# except:
#     print ("Kein Argument uebergeben")
#     sys.exit(1)

    
