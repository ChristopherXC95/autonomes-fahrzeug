#Kevin Diener 19.02.2020 Hochschule Pforzheim
#Edited by Philipp Majer 02.03.2020 - improved file handling and refactored code
#Liest die HSV-Werte aus (in h,s,v) und ignoriert den Rest
try:
    with open("/home/pi/logs/color.log","r") as fp:
        content = fp.read()
    content = content.replace('hsv(','')
    content = content.replace(')','')
    content = content.replace('%', '')
    h,s,v = [int(x) for x in content.split(',')]
    print(h,s,v)
except:
    print("Can't read file")


