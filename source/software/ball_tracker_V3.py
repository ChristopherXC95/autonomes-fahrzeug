from picar import front_wheels, back_wheels
from picar.SunFounder_PCA9685 import Servo
import picar
import time
import cv2
import numpy as np
import picar
from imutils.video import VideoStream
import numpy as np
import argparse
import cv2
import imutils
import signal
import sys


picar.setup()
# setup variables for piCar motors
bw = back_wheels.Back_Wheels()
fw = front_wheels.Front_Wheels()
bw.speed = 0
fw.turn(90)
fw.offset = 0

motor_speed = 0

#settings for camera servos
pan_servo = Servo.Servo(1)
tilt_servo = Servo.Servo(2)

pan_servo.offset = 10
tilt_servo.offset = 0

pan_servo.write(90)
tilt_servo.write(90)



# reduce resolution to
SCREEN_WIDTH = 320 #160
SCREEN_HIGHT = 240 #120

CENTER_X = SCREEN_WIDTH/2
CENTER_Y = SCREEN_HIGHT/2

# settings for ball detection
greenLower = (40, 80, 20)
greenUpper = (100, 255, 255)

# tracking parameters
BALL_MIN_SIZE = 5
BALL_TOO_TOO_FAR = 15
BALL_TOO_FAR = 20
BALL_TOO_NEAR = 50

CAMERA_STEP_X = 0
MIDDLE_TOLERANCE_X = 25
PAN_ANGLE_MAX = 170
PAN_ANGLE_MIN = 10
WAIT_X = 0

CAMERA_STEP_Y = 0
MIDDLE_TOLERANCE_Y = 25
TILT_ANGLE_MAX = 150
TILT_ANGLE_MIN = 50
WAIT_Y = 0


pan_angle = 90              # initial angle for pan
tilt_angle = 90             # initial angle for tilt
pan_servo.write(pan_angle)
tilt_servo.write(tilt_angle)

# driving parameter
FW_ANGLE_MAX    = 90+50
FW_ANGLE_MIN    = 90-50
fw_angle = 90
fw.turn(fw_angle)
MOVING = False

vc = VideoStream(src=0).start()

def destroy():
    bw.stop()
    vc.stop()
    #vc.release()

# main function
def main():

    print("Begin!")
    set_proc_name('testballtrace')
    print('Ball Track started')

    bw.stop()
    MOVING = getMovePermission()
    (greenLower,greenUpper) = getColorBoundsFromFile()

    while not signalHandler.kill_now:
        # find ball move x direction
        (x,y, radius),found_x = detectBall_V3(vc)
        if found_x:
            moveCamera_x(x,y,radius)
        
        # find ball move y direction
        (x,y, radius),found_y = detectBall_V3(vc)
        if found_y:
            moveCamera_y(x,y,radius)
        
        if MOVING:
            # only if both found move car
            if found_x and found_y: 
                moveCar(x, y, radius)
            else:
                bw.stop()
    # end programm, return to default position and turn off motors
    pan_servo.write(90)
    tilt_servo.write(90)
    fw.turn(90)
    bw.stop()
    print("Ball Track Stopped")
            
def detectBall_V3(vc):

    # grab the current frame
    try:
        frame = vc.read()
    except:
        print('camera connection lost')
        bw.stop()
        sys.exit()

    # handle the frame from VideoCapture or VideoStream
    #frame = frame[1] if args.get("video", False) else frame
    if frame is None:
        return(0,0), False
    # resize the frame, blur it, and convert it to the HSV
    # color space
    frame = imutils.resize(frame, width=320)
    blurred = cv2.GaussianBlur(frame, (11, 11), 0)
    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)

    # construct a mask for the color "green", then perform
    # a series of dilations and erosions to remove any small
    # blobs left in the mask
    mask = cv2.inRange(hsv, greenLower, greenUpper)
    mask = cv2.erode(mask, None, iterations=2)
    mask = cv2.dilate(mask, None, iterations=2)

    # find contours in the mask and initialize the current
    # (x, y) center of the ball
    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL,
        cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)
    center = None

    # only proceed if at least one contour was found
    if len(cnts) > 0:
        # find the largest contour in the mask, then use
        # it to compute the minimum enclosing circle and
        # centroid
        c = max(cnts, key=cv2.contourArea)
        ((x, y), radius) = cv2.minEnclosingCircle(c)
        M = cv2.moments(c)
        center = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))

        # only proceed if the radius meets a minimum size
        if radius > BALL_MIN_SIZE:
            # draw the circle and centroid on the frame,
            # then update the list of tracked points
            cv2.circle(frame, (int(x), int(y)), int(radius),(0, 255, 255), 2)
            cv2.circle(frame, center, 5, (0, 0, 255), -1)
    
    if (len(cnts) > 0) and (radius > BALL_MIN_SIZE):
        #only return found when radius bigger than min size
        return (x,y, radius), True
    else:
        return (0,0,0), False

def moveCamera_x(x,y, radius):
    global pan_angle, CAMERA_STEP_X, WAIT_X
    diff_horizontal = x-CENTER_X

    if abs(diff_horizontal) > MIDDLE_TOLERANCE_X:
        # ball is far enough from the middle
        if abs(diff_horizontal) > (CENTER_X/2):
            #if ball is farer than 45degree
            CAMERA_STEP_X = 8
            WAIT_X = 0.005
        else:
            CAMERA_STEP_X = 3
            WAIT_X = 0.0025
        if x < CENTER_X:
            # ball is on the left side
            pan_angle += CAMERA_STEP_X
            if pan_angle > PAN_ANGLE_MAX:
                pan_angle = PAN_ANGLE_MAX
        else:
            # ball is on the right side
            pan_angle -= CAMERA_STEP_X
            if pan_angle < PAN_ANGLE_MIN:
                pan_angle = PAN_ANGLE_MIN
        
        # turn camera towards ball
        pan_servo.write(pan_angle)
        #tilt_servo.write(tilt_angle)
        # wait
        time.sleep(WAIT_X)

def moveCamera_y(x,y, radius):
    global tilt_angle, CAMERA_STEP_Y, WAIT_Y
    diff_vertical = y-CENTER_Y

    if abs(diff_vertical) > MIDDLE_TOLERANCE_Y:
    # ball is far enough from the middle
        if abs(diff_vertical) > (CENTER_Y/2):
            #if ball is farer than 45degree
            CAMERA_STEP_Y = 8
            WAIT_Y = 0.005
        else:
            CAMERA_STEP_Y = 3
            WAIT_Y = 0.0025
        if y < CENTER_Y:
            # ball is on the bottom
            tilt_angle += CAMERA_STEP_Y
            if tilt_angle > TILT_ANGLE_MAX:
                tilt_angle = TILT_ANGLE_MAX
        else:
            # ball is on the top
            tilt_angle -= CAMERA_STEP_Y
            if tilt_angle < TILT_ANGLE_MIN:
                tilt_angle = TILT_ANGLE_MIN
        
        # turn camera towards ball
        tilt_servo.write(tilt_angle)
        # wait
        time.sleep(WAIT_Y)        

def moveCar(x,y, radius):
    global fw, bw, motor_speed, pan_angle
    
    #set motor_speed
    if(radius < BALL_TOO_TOO_FAR):
        #ball too far away, high speed
        motor_speed = 80
    elif (radius < BALL_TOO_FAR):
        #ball far away, medium speed
        motor_speed = 60
    else:
        # ball too near, slow speed for backwards
        motor_speed = 30

    #decide left or right 
    fw_angle = 180 - pan_angle
    if(fw_angle < FW_ANGLE_MIN):
        fw_angle = FW_ANGLE_MIN
    elif (fw_angle > FW_ANGLE_MAX):
        fw_angle = FW_ANGLE_MAX
    fw.turn(fw_angle)
    
    #decide forwards or backwards
    if (radius < BALL_TOO_FAR):
        #drive forward
        bw.backward()
    elif radius > BALL_TOO_NEAR:
        #drive back
        bw.forward()
    else:
        motor_speed = 0
        bw.stop()
    
    bw.speed = motor_speed
    
def getColorBoundsFromFile():
    with open("/home/pi/logs/color.log","r") as fp:
        content = fp.read()
    content = content.replace('hsv(','')
    content = content.replace(')','')
    content = content.replace('%', '')
    h,s,v = [int(x) for x in content.split(',')]
    with open("/home/pi/logs/balltolerance.log","r") as fp:
        content = fp.read()
    tol = int(content)/100.0
    
    h_low = max(0, h - tol * 128)
    h_high = min(255, h + tol *128)
    s_low = max(0, (s/100.0 - tol) * 255 )
    s_high = min(255, (s/100.0 + tol) * 255)
    v_low = max(0, (v/100.0 - tol) * 255 )
    v_high = min(255, (v/100.0 + tol) * 255)
    colLower = (h_low,s_low,v_low)
    colHigher = (h_high, s_high, v_high)
    return (colLower,colHigher)

def getMovePermission():
    with open("/home/pi/logs/moveSwitch.log",'r') as fp:
        content = fp.read()
    if content == "false":
        return False
    else:
        return True

def set_proc_name(newname):
    from ctypes import cdll, byref, create_string_buffer
    libc = cdll.LoadLibrary('libc.so.6')
    buff = create_string_buffer(len(newname)+1)
    buff.value = newname
    libc.prctl(15, byref(buff), 0, 0, 0)

class SignalHandlerClass:
    kill_now = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exitHard)
        signal.signal(signal.SIGTERM, self.exitSavely)

    def exitHard(self, signum, frame):
        bw.stop()
        sys.exit()

    def exitSavely(self, signum, frame):
        print('Shutting down savely...')
        self.kill_now = True


if __name__ == '__main__':
    signalHandler = SignalHandlerClass()
    try:
        main()
    except KeyboardInterrupt:
        destroy()
